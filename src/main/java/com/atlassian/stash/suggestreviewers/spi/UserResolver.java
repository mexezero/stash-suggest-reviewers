package com.atlassian.stash.suggestreviewers.spi;

import com.atlassian.stash.user.StashUser;

import javax.annotation.Nullable;

/**
 * A utility for resolving {@link StashUser StashUsers} from email addresses.
 *
 * @since 1.0
 */
public interface UserResolver {

    /**
     * @param emailAddresses email addresses that may or may not correspond to Stash users.
     * @return any {@link StashUser StashUsers} that match the supplied email addresses. If an email address appears
     * multiple times, the resultant {@link Iterable} will contain the corresponding {@link StashUser} multiple times.
     * Note that email addresses with no match will be disregarded, so the returned {@link Iterable} may contain fewer
     * elements than the one supplied.
     */
    Iterable<StashUser> resolve(Iterable<String> emailAddresses);

    /**
     * @param emailAddress an email address that may or may not correspond to a Stash user.
     * @return the {@link StashUser StashUser} matching the supplied email addresses, or {@code null} if the email
     * address does not match any Stash user.
     */
    @Nullable
    StashUser resolve(String emailAddress);

}
