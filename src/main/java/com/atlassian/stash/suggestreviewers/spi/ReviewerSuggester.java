package com.atlassian.stash.suggestreviewers.spi;

import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.Multimap;

/**
 * A plugin module that suggests appropriate reviewers for a set of changes.
 *
 * @since 1.0
 */
public interface ReviewerSuggester {

    /**
     * @param since the earliest {@link Changeset} that should be considered (e.g. the merge base of two branches in a
     *              pull request)
     * @param until the latest {@link Changeset} that should be considered (e.g. the tip of a topic branch being merged
     *              into master)
     * @return a {@link Multimap} of {@link StashUser users} who would be appropriate reviewers for the specified
     *         changes, and a the corresponding {@link Reason reasons} they are being suggested.
     */
    Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until);

}
