package com.atlassian.stash.suggestreviewers.internal.rest;

import com.atlassian.stash.rest.data.RestMapEntity;
import com.atlassian.stash.rest.data.RestStashUser;
import com.atlassian.stash.suggestreviewers.SuggestedReviewer;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RestSuggestedReviewer extends RestMapEntity {

    public static Function<SuggestedReviewer, RestSuggestedReviewer> TO_REST = new Function<SuggestedReviewer, RestSuggestedReviewer>() {
        @Override
        public RestSuggestedReviewer apply(SuggestedReviewer input) {
            return new RestSuggestedReviewer(input);
        }
    };

    public RestSuggestedReviewer(SuggestedReviewer suggestedReviewer) {
        put("user", new RestStashUser(suggestedReviewer.getUser()));
        put("shortReason", suggestedReviewer.getShortReason());
        put("reasons", Lists.newArrayList(suggestedReviewer.getReasons()));
    }

}
