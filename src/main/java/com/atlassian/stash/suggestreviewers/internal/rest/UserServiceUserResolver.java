package com.atlassian.stash.suggestreviewers.internal.rest;

import com.atlassian.stash.suggestreviewers.spi.UserResolver;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.google.common.collect.Lists;

import java.util.List;

public class UserServiceUserResolver implements UserResolver {

    private final UserService userService;

    public UserServiceUserResolver(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Iterable<StashUser> resolve(Iterable<String> emailAddresses) {
        List<StashUser> resolvedUsers = Lists.newArrayList();

        for (String email : emailAddresses) {
            StashUser user = resolve(email);
            if (user != null) {
                resolvedUsers.add(user);
            }
        }

        return resolvedUsers;
    }

    @Override
    public StashUser resolve(String emailAddress) {
        return userService.findUserByNameOrEmail(emailAddress);
    }
}
