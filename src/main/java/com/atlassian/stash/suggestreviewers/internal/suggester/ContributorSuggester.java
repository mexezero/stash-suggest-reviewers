package com.atlassian.stash.suggestreviewers.internal.suggester;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.AbstractChangesetCallback;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.suggestreviewers.internal.util.IntHolder;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.suggestreviewers.spi.SimpleReason;
import com.atlassian.stash.user.Person;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.stash.suggestreviewers.internal.util.StringUtils.pluralize;

public class ContributorSuggester implements ReviewerSuggester {

    private static final int SCORE_PER_COMMIT = 1000;
    private static final int MAX_COMMITS = 100;

    private final CommitService commitService;

    public ContributorSuggester(CommitService commitService) {
        this.commitService = commitService;
    }

    @Override
    public Multimap<StashUser, Reason> suggestFor(Changeset since, Changeset until) {
        ChangesetsBetweenRequest request = new ChangesetsBetweenRequest.Builder(until.getRepository())
            .include(until.getId())
            .exclude(since.getId())
            .secondaryRepository(until.getRepository())
            .build();

        final IntHolder commitCounter = new IntHolder();
        final Map<StashUser, IntHolder> contributors = Maps.newHashMap();
        commitService.streamChangesetsBetween(request, new AbstractChangesetCallback() {

            @Override
            public boolean onChangeset(@Nonnull Changeset changeset) {
                Person author = changeset.getAuthor();
                if (author instanceof StashUser) {
                    StashUser user = (StashUser) author;

                    IntHolder count = contributors.get(user);
                    if (count == null) {
                        count = new IntHolder();
                        contributors.put(user, count);
                    }
                    count.increment();
                }

                return commitCounter.increment() < MAX_COMMITS;
            }
        });

        Multimap<StashUser, Reason> suggestions = HashMultimap.create();
        for (Map.Entry<StashUser, IntHolder> entry : contributors.entrySet()) {
            int commitCount = entry.getValue().get();
            String description = commitCounter.get() < MAX_COMMITS ?
                    "Authored " + commitCount + " " + pluralize(commitCount, "commit", "commits") + " to be merged." :
                    "Authored some of the commits to be merged.";
            suggestions.put(entry.getKey(), new SimpleReason(description, commitCount * SCORE_PER_COMMIT));
        }
        return suggestions;
    }
}
