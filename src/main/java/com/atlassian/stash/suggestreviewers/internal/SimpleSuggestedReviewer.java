package com.atlassian.stash.suggestreviewers.internal;

import com.atlassian.stash.suggestreviewers.SuggestedReviewer;
import com.atlassian.stash.user.StashUser;

public class SimpleSuggestedReviewer implements SuggestedReviewer {

    private final StashUser user;
    private final String shortReason;
    private final Iterable<String> reasons;

    public SimpleSuggestedReviewer(StashUser user, String shortReason, Iterable<String> reasons) {
        this.user = user;
        this.shortReason = shortReason;
        this.reasons = reasons;
    }

    @Override
    public StashUser getUser() {
        return user;
    }

    @Override
    public String getShortReason() {
        return shortReason;
    }

    @Override
    public Iterable<String> getReasons() {
        return reasons;
    }

}
