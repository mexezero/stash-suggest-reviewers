package com.atlassian.stash.suggestreviewers.internal;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.suggestreviewers.SuggestedReviewer;
import com.atlassian.stash.suggestreviewers.SuggestedReviewerService;
import com.atlassian.stash.suggestreviewers.spi.Reason;
import com.atlassian.stash.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.stash.user.*;
import com.atlassian.stash.util.PageUtils;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ScoringSuggestedReviewerService implements SuggestedReviewerService {

    private static final Logger log = LoggerFactory.getLogger(ScoringSuggestedReviewerService.class);

    private final StashAuthenticationContext authenticationContext;
    private final CommitService commitService;
    private final MergeBaseResolver mergeBaseResolver;
    private final PermissionService permissionService;
    private final PluginAccessor pluginAccessor;

    public ScoringSuggestedReviewerService(StashAuthenticationContext authenticationContext,
                                           CommitService commitService, MergeBaseResolver mergeBaseResolver,
                                           PermissionService permissionService, PluginAccessor pluginAccessor) {
        this.authenticationContext = authenticationContext;
        this.commitService = commitService;
        this.mergeBaseResolver = mergeBaseResolver;
        this.permissionService = permissionService;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public Iterable<SuggestedReviewer> getSuggestedReviewers(Changeset fromHead, Changeset toHead, int count) {
        if (alreadyMerged(toHead, fromHead)) {
            return Collections.emptyList();
        }

        Changeset mergeBase = mergeBaseResolver.findMergeBase(fromHead, toHead);

        Multimap<StashUser, String> reasons = HashMultimap.create();
        Map<StashUser, Reason> mostRelevantReason = Maps.newHashMap();
        Map<StashUser, RankedUser> rankings = Maps.newHashMap();

        for (ReviewerSuggester suggester : pluginAccessor.getEnabledModulesByClass(ReviewerSuggester.class)) {
            try {
                Multimap<StashUser, Reason> suggestions = suggester.suggestFor(mergeBase, fromHead);
                if (suggestions == null || suggestions.isEmpty()) {
                    // guard against suggester implementations returning unexpected output
                    continue;
                }

                for (Map.Entry<StashUser, Collection<Reason>> entry : suggestions.asMap().entrySet()) {
                    if (entry.getKey() == null || entry.getValue() == null || entry.getValue().isEmpty()) {
                        // guard against suggester implementations returning unexpected output
                        continue;
                    }

                    StashUser user = entry.getKey();

                    RankedUser ranking = rankings.get(user);
                    if (ranking == null) {
                        ranking = new RankedUser(user);
                        rankings.put(user, ranking);
                    }

                    Reason best = mostRelevantReason.get(user);
                    for (Reason reason : entry.getValue()) {
                        reasons.put(user, reason.getDescription());
                        ranking.add(reason.getScore());
                        if (best == null || reason.getScore() > best.getScore()) {
                            best = reason;
                        }
                    }
                    mostRelevantReason.put(user, best);
                }
            } catch (Exception e) {
                // continue to next suggester
                log.error(suggester.getClass() + " threw an exception (or returned unexpected data) when" +
                                                           " suggesting reviewers and was ignored.", e);
            }
        }

        if (rankings.isEmpty()) {
            return Collections.emptySet();
        }

        StashUser current = authenticationContext.getCurrentUser();

        List<RankedUser> byScore = Lists.newArrayList(rankings.values());
        Collections.sort(byScore);

        List<SuggestedReviewer> suggestions = Lists.newArrayList();
        Iterator<RankedUser> iterator = byScore.iterator();
        while (iterator.hasNext() && count > 0) {
            StashUser user = iterator.next().getUser();
            if (current != null && StashUserEquality.equals(current, user)) {
                //Don't suggest the authenticated user; they can't review their own work
                continue;
            }

            if (permissionService.hasRepositoryPermission(user, fromHead.getRepository(), Permission.REPO_READ)) {
                suggestions.add(new SimpleSuggestedReviewer(user,
                        mostRelevantReason.get(user).getShortDescription(),
                        reasons.get(user)));
                count--;
            }
        }

        return suggestions;
    }

    private boolean alreadyMerged(Changeset since, Changeset until) {
        ChangesetsBetweenRequest request = new ChangesetsBetweenRequest.Builder(since.getRepository())
                .include(until.getId())
                .exclude(since.getId())
                .secondaryRepository(until.getRepository())
                .build();
        return commitService.getChangesetsBetween(request, PageUtils.newRequest(0, 1)).getSize() == 0;
    }

}