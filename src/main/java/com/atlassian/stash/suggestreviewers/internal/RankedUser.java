package com.atlassian.stash.suggestreviewers.internal;

import com.atlassian.stash.user.StashUser;

public class RankedUser implements Comparable<RankedUser> {

    private final StashUser user;
    private int score;

    public RankedUser(StashUser user) {
        this.user = user;
    }

    public StashUser getUser() {
        return user;
    }

    public void add(int score) {
        this.score += score;
    }

    @Override
    public int compareTo(RankedUser other) {
        return other.score - score;
    }
}
