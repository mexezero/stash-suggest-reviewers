package com.atlassian.stash.suggestreviewers.internal.suggester.git;

import java.util.Set;

public interface DiffSummary {

    Set<String> getCreatedPaths();

    Set<String> getDeletedPaths();

}
