package com.atlassian.stash.suggestreviewers.internal.rest;

import com.atlassian.stash.rest.exception.UnhandledExceptionMapper;
import com.atlassian.stash.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class SuggestedReviewersExceptionMapper extends UnhandledExceptionMapper {

    public SuggestedReviewersExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }

}
