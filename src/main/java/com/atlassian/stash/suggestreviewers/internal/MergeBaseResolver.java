package com.atlassian.stash.suggestreviewers.internal;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;
import com.atlassian.stash.scm.git.GitScmConfig;
import com.atlassian.stash.scm.git.merge.GitMergeBaseBuilder;
import com.atlassian.stash.suggestreviewers.internal.suggester.git.FirstLineOutputHandler;
import com.atlassian.stash.suggestreviewers.internal.util.GitUtils;

/**
 * Determines the merge base of a pair of commits.
 */
public class MergeBaseResolver {

    private final GitCommandBuilderFactory builderFactory;
    private final GitScmConfig config;
    private final CommitService commitService;

    public MergeBaseResolver(GitCommandBuilderFactory builderFactory, GitScmConfig config, CommitService commitService) {
        this.builderFactory = builderFactory;
        this.config = config;
        this.commitService = commitService;
    }

    public Changeset findMergeBase(Changeset a, Changeset b) {
        GitMergeBaseBuilder builder = builderFactory.builder(a.getRepository())
                .mergeBase()
                .between(a.getId(), b.getId());
        GitUtils.setAlternateIfCrossRepository(builder, a.getRepository(), b.getRepository(), config);

        String sha = builder.build(new FirstLineOutputHandler()).call();
        if (sha == null) {
            return null;
        }

        return commitService.getChangeset(a.getRepository(), sha);
    }
}
