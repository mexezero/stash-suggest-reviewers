package com.atlassian.stash.suggestreviewers.internal.suggester.git;

import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.scm.DefaultCommandExitHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Custom {@link com.atlassian.stash.scm.CommandExitHandler} for that looks for a particular error message returned by
 * {@code git-blame} when a non-existent path is specified.
 */
public class MissingPathIgnoringExitHandler extends DefaultCommandExitHandler {

    public MissingPathIgnoringExitHandler(I18nService i18nService) {
        super(i18nService);
    }

    @Override
    public void onError(@Nonnull String command, int exitCode, @Nullable String stdErr, @Nullable Throwable thrown) {
        // Check if the error message is the standard output given if the path doesn't exist - ignore it if so
        if (stdErr != null && stdErr.startsWith("fatal: no such path")) {
            // ok, fair enough
            return;
        }

        // Otherwise, something else went wrong - fail.
        super.onError(command, exitCode, stdErr, thrown);
    }

}
