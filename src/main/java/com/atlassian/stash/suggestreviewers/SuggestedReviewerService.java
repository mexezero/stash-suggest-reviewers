package com.atlassian.stash.suggestreviewers;

import com.atlassian.stash.content.Changeset;

/**
 * Suggests appropriate reviewers for a set of {@link Changeset changes}.
 *
 * @since 1.0
 */
public interface SuggestedReviewerService {

    Iterable<SuggestedReviewer> getSuggestedReviewers(Changeset fromHead, Changeset toHead, int count);

}