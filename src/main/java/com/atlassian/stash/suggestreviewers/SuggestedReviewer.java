package com.atlassian.stash.suggestreviewers;

import com.atlassian.stash.user.StashUser;

/**
 * A {@link StashUser} who has been suggested as a reviewer.
 *
 * @since 1.0
 */
public interface SuggestedReviewer {

    /**
     * @return the {@link StashUser}
     */
    StashUser getUser();

    /**
     * @return a short description of the primary reason the user was suggested as a reviewer.
     */
    String getShortReason();

    /**
     * <strong>This is not displayed to the user in version 1.0, but may be in a future release.</strong>
     *
     * @return longer descriptions of all the reasons the user was suggested as a reviewer.
     */
    Iterable<String> getReasons();

}
