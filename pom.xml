<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.11</version>
    </parent>

    <groupId>com.atlassian.stash.plugin</groupId>
    <artifactId>stash-suggest-reviewers</artifactId>
    <version>1.5-SNAPSHOT</version>
    <name>Stash Reviewer Suggester</name>
    <description>Provides intelligent reviewer suggestions for Pull Requests.</description>
    <packaging>atlassian-plugin</packaging>

    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/</url>
    </organization>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/stash-suggest-reviewers.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/stash-suggest-reviewers.git</developerConnection>
        <url>https://bitbucket.org/atlassian/stash-suggest-reviewers</url>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <!-- Override properties from base-pom to use/require Java 7 -->
        <jdkLevel>1.7</jdkLevel>
        <maven.compiler.source>7</maven.compiler.source>
        <maven.compiler.target>7</maven.compiler.target>
        <requireJavaVersion>1.7.0</requireJavaVersion>

        <stash.version>3.0.5</stash.version>
        <stash.data.version>${stash.version}</stash.data.version>
        <amps.version>5.0.3</amps.version>
        <plugin.testrunner.version>1.1</plugin.testrunner.version>
    </properties>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.1</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-stash-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Atlassian-Plugin-Key>${project.groupId}.${project.artifactId}</Atlassian-Plugin-Key>
                        <Export-Package>
                            com.atlassian.stash.suggestreviewers,
                            com.atlassian.stash.suggestreviewers.spi
                        </Export-Package>
                        <Import-Package>
                            com.atlassian.stash*,
                            com.atlassian.fugue,
                            com.atlassian.plugin*,
                            com.atlassian.plugin.osgi.bridge.external,
                            com.atlassian.plugin.osgi.external,
                            com.atlassian.plugins.rest.common.security,
                            com.google.common.base,
                            com.google.common.collect,
                            com.sun.jersey.spi.resource,
                            javax.annotation,
                            javax.ws.rs,
                            javax.ws.rs.core,
                            javax.ws.rs.ext,
                            org.codehaus.jackson.map.annotate,
                            org.slf4j,
                        </Import-Package>
                        <Private-Package>
                            com.atlassian.stash.suggestreviewers.internal*
                        </Private-Package>
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                    <products>
                        <product>
                            <id>stash</id>
                            <instanceId>stash</instanceId>
                            <version>${stash.version}</version>
                            <dataVersion>${stash.data.version}</dataVersion>
                            <containerId>tomcat7x</containerId>
                        </product>
                    </products>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.stash</groupId>
                <artifactId>stash-parent</artifactId>
                <version>${stash.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-scm-git-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-scm-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-util</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-page-objects</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

</project>
